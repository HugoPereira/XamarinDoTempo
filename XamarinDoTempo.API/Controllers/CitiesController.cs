﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XamarinDoTempo.API.Models;

namespace XamarinDoTempo.API.Controllers
{
    public class CitiesController : ApiController
    {
        private DataContextDataContext dc = new DataContextDataContext();
        // GET: api/Cities
        public async Task<IHttpActionResult> GetCities()
        {
            var lista = dc.cities.ToList();

            if (lista.Count == 0)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, lista));
        }

        // GET: api/Cities/5
        public async Task<IHttpActionResult> GetCitiesByCountry(string country)
        {
            var lista = dc.cities.Where(x => x.country == country).ToList();

            if (lista.Count == 0)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, lista));
        }
    }
}
