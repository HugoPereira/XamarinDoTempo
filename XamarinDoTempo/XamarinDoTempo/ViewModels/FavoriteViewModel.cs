﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinDoTempo.Models;
using XamarinDoTempo.Services;

namespace XamarinDoTempo.ViewModels
{
    public class FavoriteViewModel : BaseViewModel
    {
        #region services

        private ApiServices _apiService;

        private DataServices _dataServices;


        #endregion

        #region atributes



        //lista para as telemovel, apenas usando no viewmodel
        //private ObservableCollection<Country> _countries;

        private ObservableCollection<CityItemViewModel> _cities;

        private bool _isRefreshing;
        //deixou de ser local e passou a ser global(mainviewmodel)
        //private List<Country> _coutriesList;


        private string _filter;

        #endregion

        #region command

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadCities); }

        }

        public ICommand SearchCommand
        {
            get { return new RelayCommand(search); }

        }


        private async void search()
        {
           
                try
                {
                    List<City> response = this._dataServices.Get<City>(false).ToList();

                    if (response == null)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                             "Error",
                             "You dont have favorite cities",
                             "Ok");

                        return;
                    }


                    MainViewModel.GetInstance().CitiesList = response;
                }
                catch
                {
                    //this.CitiesList = null;
                }
           
                this.FavoriteCitiesList = new ObservableCollection<CityItemViewModel>(
                    this.ToCityItemViewModel().Where(
                        c => c.Name.ToLower().Contains(this.Filter.ToLower())
                    ));
            
        }

        private IEnumerable<CityItemViewModel> ToCityItemViewModel()
        {
            //cada pais converte num novo coutryviewmodel
            return MainViewModel.GetInstance().FavoriteCitiesList.Select(c => new CityItemViewModel
            {
                Id = c.Id,
                Name = c.Name,
                Country = c.Country,
                lat = c.lat,
                lon = c.lon
            });
        }

        #endregion

        #region properties



        public string Filter
        {
            get { return this._filter; }

            set
            {
                SetValue(ref this._filter, value);
                this.search();
            }
        }


        public bool IsRefreshing
        {
            get { return this._isRefreshing; }

            set
            {
                SetValue(ref this._isRefreshing, value);
            }
        }


        public ObservableCollection<CityItemViewModel> FavoriteCitiesList
        {
            get { return this._cities; }

            set
            {
                SetValue(ref this._cities, value);
            }
        }



        #endregion

        public FavoriteViewModel()
        {
            this._dataServices = new DataServices();
            this._apiService = new ApiServices();

            this.LoadCities();
        }


        //carrega os paises
        public async void LoadCities()
        {
            IsRefreshing = true;

            List<City> response = this._dataServices.Get<City>(false).ToList();

            if (response.Count == 0)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You dont have favorite cities",
                    "Ok");

                return;
            }


            MainViewModel.GetInstance().FavoriteCitiesList = response;


            this.FavoriteCitiesList = new ObservableCollection<CityItemViewModel>(this.ToCityItemViewModel());


            IsRefreshing = false;
        }

       
    }
}
