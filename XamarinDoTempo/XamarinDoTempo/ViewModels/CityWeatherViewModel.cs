﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;
using XamarinDoTempo.Models;
using XamarinDoTempo.Services;

namespace XamarinDoTempo.ViewModels
{
    public class CityWeatherViewModel : BaseViewModel
    {
        private ApiServices _apiService;

        public City City { get; set; }

        public WeatherForecast WeatherForecast { get; set; }

        private bool _isEnable;



        public bool IsEnable
        {
            get { return _isEnable; }
            set { SetValue(ref this._isEnable, value); }
        }

        private bool _isRefreshing;

        public bool IsRefreshing
        {
            get { return this._isRefreshing; }

            set
            {
                SetValue(ref this._isRefreshing, value);
            }
        }


        private string _vod;

        private double _message;

        private int _cnt;

        private string _temp;
        public string Temp
        {
            get { return _temp; }
            set { SetValue(ref this._temp, value); }
        }

        private string _condition;
        public string Condition
        {
            get { return _condition; }
            set { SetValue(ref this._condition, value); ; }
        }

        private string _wind;
        public string Wind
        {
            get { return _wind; }
            set { SetValue(ref this._wind, value); ; }
        }

        private string _humidity;
        public string Humidity
        {
            get { return _humidity; }
            set { SetValue(ref this._humidity, value); }
        }

        private string _minTemp;
        public string MinTemperature
        {
            get { return _minTemp; }
            set { SetValue(ref this._minTemp, value); }
        }

        private string _maxTemp;
        public string MaxTemperature
        {
            get { return _maxTemp; }
            set { SetValue(ref this._maxTemp, value); }
        }

        private string _pressure;
        public string Pressure
        {
            get { return _pressure; }
            set { SetValue(ref this._pressure, value); }
        }

        private string _date;
        public string Date
        {
            get { return _date; }
            set { SetValue(ref this._date, value); }
        }

        private string _displayTemp;
        public string DisplayTemp
        {
            get { return _displayTemp; }
            set { SetValue(ref this._displayTemp, value); }
        }

        private ObservableCollection<Weather> _weather;

        private ObservableCollection<WeatherRoot> _items;

        public ObservableCollection<Weather> Weather
        {
            get { return _weather; }

            set { SetValue(ref this._weather, value); }
        }

        public ObservableCollection<WeatherRoot> Items
        {
            get { return _items; }

            set { SetValue(ref this._items, value); }
        }


        public string Vod
        {
            get { return _vod; }

            set { SetValue(ref this._vod, value); }
        }

        public double Message
        {
            get { return _message; }

            set { SetValue(ref this._message, value); }
        }

        public int Cnt
        {
            get { return _cnt; }

            set { SetValue(ref this._cnt, value); }
        }

        public CityWeatherViewModel(City city)
        {
            this.City = city;
            
            _apiService = new ApiServices();

            this.IsEnable = false;

            LoadWeather();

        }

        private async void LoadWeather()
        {
            this.IsEnable = false;

            //testar a conexao
            var connection = await this._apiService.CheckConnection();
            //se nao tiver conexao
            if (!connection.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert("Error", $"{connection.Message}: The weather is not avaiable! turn back the connection", "Ok");

                //volta a pagina atras
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            
            var response = await this._apiService.GetListWeather<WeatherForecast>("http://api.openweathermap.org", "/data/2.5/", "forecast?id=", City.Id);

            if (!response.IsSucess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Ok");

                return;
            }

            MainViewModel.GetInstance().WeatherList = (WeatherForecast)response.Result;

            this.WeatherForecast = (WeatherForecast)response.Result;

            this.Items = new ObservableCollection<WeatherRoot>(MainViewModel.GetInstance().WeatherList.Items);

            this.Message = MainViewModel.GetInstance().WeatherList.Message;

            this.Cnt = MainViewModel.GetInstance().WeatherList.Cnt;

            this.Vod = MainViewModel.GetInstance().WeatherList.Vod;

            this.Weather = new ObservableCollection<Weather>(Items[0].Weather);

            Temp = $"Temp: {Items[0]?.MainWeather?.Temperature ?? 0}° C";
            Condition = $"{City.Name}: {Items[0]?.Weather?[0]?.Description ?? string.Empty}";
            Humidity = $"Humidity: {Items[0]?.MainWeather?.Humidity} %";
            MinTemperature = $"Min Temperature: {Items[0]?.MainWeather?.MinTemperature}° C";
            MaxTemperature = $"Max Temperature: {Items[0]?.MainWeather?.MaxTemperature}° C";
            Pressure = $"Pressure: {Items[0]?.MainWeather?.Pressure} %";
            Wind = $"Wind: {Items[0].Wind.Speed} Km/h - Direction: {Items[0].Wind.WindDirectionDegrees}° ";

            Date = DateTime.Parse(Items[0].Date).ToLocalTime().ToString("g");

            DisplayTemp = $"Temp: {Items[0].MainWeather?.Temperature ?? 0}° {Items[0].Weather?[0]?.Main ?? string.Empty}";

            this.IsEnable = true;
        }

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadWeather); }

        }

    }
}
