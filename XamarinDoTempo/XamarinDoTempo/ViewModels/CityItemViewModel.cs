﻿namespace XamarinDoTempo.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;
    using Models;
    using Views;
    using XamarinDoTempo.Services;

    public class CityItemViewModel: City
    {
        private DataServices _dataServices = new DataServices();

        public ICommand SelectCityCommand
        {
            get { return new RelayCommand(SelectCity); }
        }

        public string NameCity
        {
            get { return string.Format("{0}, {1}", this.Name, this.Country); }
            
        }

        private async void SelectCity()
        {

            City city = new City {
                Id = this.Id,
                Name = this.Name,
                Country = this.Country
            };
            
            _dataServices.InsertOrUpdate(city);

            MainViewModel.GetInstance().CityWeather = new CityWeatherViewModel(this);

            await Application.Current.MainPage.Navigation.PushAsync(new CityWeatherTabbedPage());
            
        }
    }
}
