﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;
using XamarinDoTempo.Models;
using XamarinDoTempo.Services;
using XamarinDoTempo.Views;

namespace XamarinDoTempo.ViewModels
{
	public class RegisterViewModel : BaseViewModel
	{
        #region attributes

        private bool _isRunning;

	    private bool _isVisible;

        private bool _isEnabled;

        private string _email;

        private string _password;

        private string _confirmPassword;

        //apiservice
        private ApiServices _apiService;




        #endregion


        #region properties

	    public bool IsVisible
	    {
	        get { return _isVisible; }

	        set
	        {
	            //chamamos o setvalue do baseviewmodel e passamos o parametros
	            SetValue(ref this._isVisible, value);
	        }
	    }


        public bool IsRunning
        {
            get { return _isRunning; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._isRunning, value);
            }
        }
        
        public bool IsEnabled
        {
            get { return _isEnabled; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._isEnabled, value);
            }
        }


        public string Email
        {
            get { return _email; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._email, value);
            }
        }


        public string Password
        {
            get { return _password; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._password, value);
            }
        }


        public string ConfirmPassword
        {
            get { return _confirmPassword; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._confirmPassword, value);
            }
        }

        #endregion


        #region commands

        public ICommand RegisterCommand
        {
            //chama o comando login, sem o nome command e dps gerar metodo
            get { return new RelayCommand(Register); }
        }


        private async void Register()
        {
            IsVisible = true;

            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "You must enter email, please!", "Ok");

                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "You must enter password, please!", "Ok");

                return;
            }

            if (string.IsNullOrEmpty(this.ConfirmPassword))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "You must Confirm password, please!", "Ok");

                return;
            }

            if (this.Password != this.ConfirmPassword)
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "Password don´t match!", "Ok");

                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;


            //****************autenticacao usando token**************************

            //verifica se existe conexao
            var connection = await this._apiService.CheckConnection();

            if (!connection.IsSucess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "ok");

                return;
            }
            User myUser = new User
            {
                Email = this.Email,
                Password = this.Password,
                ConfirmPassword = this.ConfirmPassword
            };
            
            try
            {
               var response = await _apiService.AddModelAsync("https://xamarindotempoapi.azurewebsites.net/", "api", "/Account/Register", myUser);

                if (response.IsSucess)
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Info",
                        "Your now Registered",
                        "Ok");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Info",
                        "Your Registered is Cancelled",
                        "Ok");
                }

            }
            catch (Exception e)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    e.Message,
                    "Ok");
            }

            

            //para nao estar spr a chamar o mainviewmodel.getinstance
            var mainViewModel = MainViewModel.GetInstance();
            //atribuo a mainviewmodel para user em qq lado o token

            this.IsRunning = false;
            this.IsEnabled = true;
            this.IsVisible = false;

            this.Email = string.Empty;
            this.Password = string.Empty;
            this.ConfirmPassword = string.Empty;

            

            //mainViewModel.Initial = new InitialViewModel();

            try
            {
                await Application.Current.MainPage.Navigation.PopAsync();
            }
            catch 
            {

            }
             
        }

        public ICommand CancelCommand
        {
            //chama o comando login, sem o nome command e dps gerar metodo
            get { return new RelayCommand(Cancel); }
        }


        private async void Cancel()
        {
            //MainViewModel.GetInstance().Initial = new InitialViewModel();
            try
            {
                await Application.Current.MainPage.Navigation.PopAsync();
            }
            catch
            {

            }

        }

        #endregion
        
        #region contructors

        public RegisterViewModel()
        {
            //apiservice
            this._apiService = new ApiServices();
            
            this.IsEnabled = true;
            this.IsVisible = false;

        }



        #endregion
    }
}