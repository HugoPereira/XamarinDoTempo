﻿namespace XamarinDoTempo.ViewModels
{
    using System.Collections.Generic;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services;
    using Xamarin.Forms;
    using XamarinDoTempo.Views;

    public class MainViewModel
    {
        #region properties

        public TokenResponse Token { get; set; }

        public List<Weather> WeatherRootList { get; set; }

        public WeatherForecast WeatherList { get; set; }

        public List<City> CitiesList { get; set; }

        public List<City> FavoriteCitiesList { get; set; }

        public ApiServices _apiService;

        public DataServices _dataService;

        #endregion

        #region ViewModel



        public RegisterViewModel Register { get; set; }

        public LoginViewModel Login { get; set; }

        public CityViewModel Cities { get; set; }

        public InitialViewModel Initial { get; set; }

        public CityWeatherViewModel CityWeather { get; set; }

        public FavoriteViewModel Favorite { get; set; }

       

        #endregion


        #region Contructors

        public MainViewModel()
        {
            //digo que ela e igual a ela propria
            instance = this;

            this.Initial = new InitialViewModel();

            _apiService = new ApiServices();

            _dataService = new DataServices();

        }

        #endregion


        #region singleton
        //design pattern singleton

        //instancio o proprio objecto, estatico para ser visto em qq lado
        private static MainViewModel instance;

        //crio um metodo estatico, onde ele se envia a ele proprio
        public static MainViewModel GetInstance()
        {
            //se a instancia nao existe crio-a
            if (instance == null)
            {
                return new MainViewModel();
            }

            //senao retorno-a
            return instance;
        }


        #endregion


        public ICommand LogoutCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(Logout); }
        }

        private async void Logout()
        {
            try
            {
                _dataService.DeleteAll<User>();

                await Application.Current.MainPage.Navigation.PushAsync(new InitialPage());
            }
            catch
            {
            }
           
        }

        public ICommand AboutCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(About); }
        }

        private async void About()
        {
            try
            {
                await Application.Current.MainPage.Navigation.PushAsync(new AboutPage());
            }
            catch 
            {
            }
            
        }

        public ICommand AddFavoriteCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(FavoritePage); }
        }

        private async void FavoritePage()
        {
            try
            {
                GetInstance().Cities = new CityViewModel();

                await Application.Current.MainPage.Navigation.PushAsync(new CityPage());
            }
            catch
            {
            }


        }

        public ICommand GoFavoritePageCommand
        {
            // é necessário o Nugt MvvmLightLibs
            // esta livraria deteta o comando e manda para o metodo
            get { return new RelayCommand(GoFavoritePage); }
        }

        private async void GoFavoritePage()
        {
            try
            {
                GetInstance().Favorite = new FavoriteViewModel();

                await Application.Current.MainPage.Navigation.PushAsync(new FavoritePage());
            }
            catch
            {
            }


        }

    }
}