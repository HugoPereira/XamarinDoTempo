﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using XamarinDoTempo.Models;
using XamarinDoTempo.Services;
using XamarinDoTempo.Views;

namespace XamarinDoTempo.ViewModels
{
	public class LoginViewModel : BaseViewModel
	{
	    
	    #region attributes

	    private bool _isRunning;

	    private bool _isRemembered;

	    private bool _isEnabled;

	    private string _email;

	    private string _password;

	    private bool _isVisible;

	    //apiservice
	    private ApiServices _apiService;

	    private DataServices _dataServices;
        
        #endregion

        #region properties

        public bool IsRunning
        {
            get { return _isRunning; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._isRunning, value);
            }
        }


        public bool IsRemembered
        {
            get { return _isRemembered; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._isRemembered, value);
            }
        }


        public bool IsEnabled
        {
            get { return _isEnabled; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._isEnabled, value);
            }
        }


        public string Email
        {
            get { return _email; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._email, value);
            }
        }


        public string Password
        {
            get { return _password; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._password, value);
            }
        }

	    public bool IsVisible
	    {
	        get { return _isVisible; }

	        set
	        {
	            //chamamos o setvalue do baseviewmodel e passamos o parametros
	            SetValue(ref this._isVisible, value);
	        }
	    }

        #endregion


        #region commands


	    public ICommand CancelCommand
	    {
	        //chama o comando login, sem o nome command e dps gerar metodo
	        get { return new RelayCommand(Cancel); }
	    }


	    private async void Cancel()
	    {
            //MainViewModel.GetInstance().Initial = new InitialViewModel();
            try
            {
                await PopupNavigation.PopAsync(true);
            }
            catch
            {

            }
            
	    }


        //buscar nuget mvvm.lightslib
        public ICommand LoginCommand
        {
            //chama o comando login, sem o nome command e dps gerar metodo
            get { return new RelayCommand(Login); }
        }

        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "You must enter email, please!", "Ok");

                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Error",
                    "You must enter password, please!", "Ok");

                return;
            }

            this.IsVisible = true;
            this.IsRunning = true;
            this.IsEnabled = false;

            //****************autenticacao usando token**************************
            var mainViewModel = MainViewModel.GetInstance();

            //verifica se existe conexao
            var connection = await this._apiService.CheckConnection();



            if (!connection.IsSucess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
               
                
                User user = _dataServices.First<User>(false);

                if (user == null)
                {
                    await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    $"{connection.Message} You dont have any saved user",
                    "ok");

                    try
                    {
                        await PopupNavigation.PopAsync(true);
                        return;
                    }
                    catch
                    {
                    }
                    
                }
               

                if (this.Email == user.Email && this.Password == user.Password)
                {
                    this.IsVisible = false;
                    this.IsRunning = false;
                    this.IsEnabled = true;

                    this.Email = string.Empty;
                    this.Password = string.Empty;

                    City city2 = _dataServices.First<City>(false);

                    if (city2 != null)
                    {
                        MainViewModel.GetInstance().Favorite = new FavoriteViewModel();

                        await Application.Current.MainPage.Navigation.PushAsync(new FavoritePage());

                        await Application.Current.MainPage.Navigation.PopPopupAsync();
                    }
                    else
                    {
                        MainViewModel.GetInstance().Cities = new CityViewModel();

                        await Application.Current.MainPage.Navigation.PushAsync(new CityPage());

                        await Application.Current.MainPage.Navigation.PopPopupAsync();
                    }
                }
            }
            else
            {
                //cria o caminho para o token
                var token = await this._apiService.GetToken("https://xamarindotempoapi.azurewebsites.net/",
                    this.Email,
                    this.Password);

                
                //ve se e nulo
                if (token == null)
                {
                    this.IsVisible = false;
                    this.IsRunning = false;
                    this.IsEnabled = true;

                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        "Something was wrong, please try later.",
                        "ok");

                    return;

                }

                //ve se vem vazio
                if (string.IsNullOrEmpty(token.AccessToken))
                {
                    this.IsVisible = false;
                    this.IsRunning = false;
                    this.IsEnabled = true;

                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        token.ErrorDescription,
                        "ok");

                    return;
                }

                User user2 = _dataServices.First<User>(false);

                if (user2 == null)
                {
                    try
                    {
                        _dataServices.DeleteAll<User>();
                        _dataServices.DeleteAll<City>();
                    }
                    catch
                    {
                    }
                }
                else
                {
                    if (user2.Email != this.Email)
                    {
                        try
                        {
                            _dataServices.DeleteAll<User>();
                            _dataServices.DeleteAll<City>();
                        }
                        catch
                        {
                        }

                    }
                }


                //if (user2 != null)
                //{
                   
                //}

               

                if (IsRemembered)
                {
                    User user = new User
                    {
                        UserId = 1,
                        Email = this.Email,
                        Password = this.Password,
                        ConfirmPassword = this.Password
                    };

                    _dataServices.InsertOrUpdate(user);
                }
                else
                {
                    _dataServices.DeleteAll<User>();
                }

                MainViewModel.GetInstance().Token = token;
                mainViewModel.Token = token;

               

                this.IsVisible = false;
                this.IsRunning = false;
                this.IsEnabled = true;

                this.Email = string.Empty;
                this.Password = string.Empty;



                City favorite = _dataServices.First<City>(false);

                if (favorite != null)
                {
                    MainViewModel.GetInstance().Favorite = new FavoriteViewModel();

                    await Application.Current.MainPage.Navigation.PushAsync(new FavoritePage());

                    await Application.Current.MainPage.Navigation.PopPopupAsync();
                }
                else
                {
                    MainViewModel.GetInstance().Cities = new CityViewModel();

                    await Application.Current.MainPage.Navigation.PushAsync(new CityPage());

                    await Application.Current.MainPage.Navigation.PopPopupAsync();
                }

            }
            
        }

#endregion

        #region contructors

        public LoginViewModel()
        {
            //apiservice
            this._apiService = new ApiServices();
            this._dataServices = new DataServices();
            
            this.IsRemembered = true;

            this.IsVisible = false;
            this.IsRunning = false;
            this.IsEnabled = true;

            LoadCities();

            LoadRemember();
        }

        private void LoadRemember()
        {
            User user = _dataServices.First<User>(false);
            if (user != null)
            {
                this.Email = user.Email;
                this.Password = user.Password;
            }
            else
            {
                this.Email = string.Empty;
                this.Password = string.Empty;
            }
        }


        private async void LoadCities()
        {
            //testar a conexao
            var connection = await this._apiService.CheckConnection();
            //se nao tiver conexao
            if (!connection.IsSucess)
            {
                //await Application.Current.MainPage.DisplayAlert("Error", connection.Message, "Ok");
                await Application.Current.MainPage.DisplayAlert("Error", $"{connection.Message}:  Cities was not avaiable! turn back the connection", "Ok");


                //volta a pagina atras
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            try
            {
                var response = await this._apiService.GetList<City>("http://xamarindotempoapi.azurewebsites.net", "/api/", "Cities");
                if (!response.IsSucess)
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        response.Message,
                        "Ok");

                    return;
                }

                MainViewModel.GetInstance().CitiesList = (List<City>)response.Result;


            }
            catch
            {
            }


        }
        #endregion
    }
}