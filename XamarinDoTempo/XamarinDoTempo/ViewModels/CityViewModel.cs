﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using XamarinDoTempo.Models;
using XamarinDoTempo.Services;
using XamarinDoTempo.Views;

namespace XamarinDoTempo.ViewModels
{
    public class CityViewModel: BaseViewModel
    {
        #region services

        private ApiServices _apiService;


        #endregion

        #region atributes



        //lista para as telemovel, apenas usando no viewmodel
        //private ObservableCollection<Country> _countries;

        private ObservableCollection<CityItemViewModel> _cities;

        private bool _isRefreshing;
        //deixou de ser local e passou a ser global(mainviewmodel)
        //private List<Country> _coutriesList;
        private bool _isEnabled;

        private string _filter;

        #endregion

        #region command

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(LoadCities); }

        }

        public ICommand SearchCommand
        {
            get { return new RelayCommand(search); }

        }


        private void search()
        {
            if (this.Filter.Length < 2)
            {
                this.CitiesList = new ObservableCollection<CityItemViewModel>(this.ToCityItemViewModel());
            }
            else
            {
                this.CitiesList = new ObservableCollection<CityItemViewModel>(
                    this.ToCityItemViewModel().Where(
                        c => c.Name.ToLower().Contains(this.Filter.ToLower())
                    ));
            }
        }

        private List<City> Cities = MainViewModel.GetInstance().CitiesList;

        private IEnumerable<CityItemViewModel> ToCityItemViewModel()
        {
            //cada pais converte num novo coutryviewmodel
            return Cities.Select(c => new CityItemViewModel
            {
                Id = c.Id,
                Name = c.Name,
                Country = c.Country
            });
        }

        #endregion

        #region properties
       


        public string Filter
        {
            get { return this._filter; }

            set
            {
                SetValue(ref this._filter, value);
                this.search();
            }
        }


        public bool IsRefreshing
        {
            get { return this._isRefreshing; }

            set
            {
                SetValue(ref this._isRefreshing, value);
            }
        }

        public bool IsEnabled
        {
            get { return this._isEnabled; }

            set
            {
                SetValue(ref this._isEnabled, value);
            }
        }


        public ObservableCollection<CityItemViewModel> CitiesList
        {
            get { return this._cities; }

            set
            {
                SetValue(ref this._cities, value);
            }
        }



        #endregion

        public CityViewModel()
        {

            this._apiService = new ApiServices();

            this.LoadCities();
        }
        

        //carrega os paises
        private async void LoadCities()
        {
            IsRefreshing = true;
            IsEnabled = false;
          

           // MainViewModel.GetInstance().CitiesList = (List<City>)response.Result;


            //this.CitiesList = new ObservableCollection<CityItemViewModel>(this.ToCityItemViewModel());

            //MainViewModel.GetInstance().CitiesList = (List<City>)cities;


            this.CitiesList = new ObservableCollection<CityItemViewModel>(this.ToCityItemViewModel());

            //this.CitiesList = null;

            IsEnabled = true;
            IsRefreshing = false;
        }
    }
}

