﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using XamarinDoTempo.Views;

namespace XamarinDoTempo.ViewModels
{
	public class InitialViewModel : BaseViewModel
    {
        //private bool _isRunning;

        private bool _isEnabled;



        //public bool IsRunning
        //{
        //    get { return _isRunning; }

        //    set
        //    {
        //        SetValue(ref this._isRunning, value);
        //    }
        //}

        public bool IsEnabled
        {
            get { return _isEnabled; }

            set
            {
                //chamamos o setvalue do baseviewmodel e passamos o parametros
                SetValue(ref this._isEnabled, value);
            }
        }

        public InitialViewModel()
        {
            IsEnabled = true;
            //IsRunning = true;
        }

        public ICommand RegisterCommand
        {
            //chama o comando login, sem o nome command e dps gerar metodo
            get { return new RelayCommand(Register); }
        }

        public ICommand LoginCommand
        {
            get { return new RelayCommand(Login); }
        }

        private async void Login()
        {

            MainViewModel.GetInstance().Login = new LoginViewModel();
            //await Application.Current.MainPage.Navigation.PushAsync(PopupNavigation.PushAsync(new LoginPage()));
            await PopupNavigation.PushAsync(new LoginPage());
        }

        private async void Register()
        {
            MainViewModel.GetInstance().Register = new RegisterViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
        }
    }
}