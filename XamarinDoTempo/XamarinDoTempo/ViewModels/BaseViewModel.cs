﻿namespace XamarinDoTempo.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        //metodos genericos, passo uma propriedade, que nao sei qual vai ser(meter callermembername
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            //invocar o evento da mudanca de propriedade
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)); //delegates
        }

        //lista generica T, pode ser qq obj   referencia, valor e propriedade
        protected void SetValue<T>(ref T backingField, T value, [CallerMemberName] string propertyName = null)
        {
            //verifico se sao do mesmo tipo e se houve alteracao do valor
            if (EqualityComparer<T>.Default.Equals(backingField, value))
            {
                return;
            }

            //se forem digo que e igual a value
            backingField = value;

            //invoco o metodo e altero a propriedade
            OnPropertyChanged(propertyName);
        }
    }
}
