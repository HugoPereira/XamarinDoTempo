﻿
namespace XamarinDoTempo.Infastructure
{
    using ViewModels;

    public class InstanceLocator
    {
        #region Properties

        public MainViewModel Main { get; set; }

        #endregion

        #region Constructors

        public InstanceLocator()
        {
            //instancio a mainviewmodel e cria-a
            this.Main = new MainViewModel();
        }

        #endregion


    }
}