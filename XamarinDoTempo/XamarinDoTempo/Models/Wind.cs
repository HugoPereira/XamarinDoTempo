﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace XamarinDoTempo.Models
{
    public class Wind
    {
        [JsonProperty("speed")]
        public double Speed { get; set; } 

        [JsonProperty("deg")]
        public double WindDirectionDegrees { get; set; } 

    }
}
