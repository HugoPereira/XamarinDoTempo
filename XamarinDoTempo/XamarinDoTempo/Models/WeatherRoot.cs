﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace XamarinDoTempo.Models
{
    public class WeatherRoot
    {
        [JsonProperty("coord")]
        public Coord Coordinates { get; set; } 

        [JsonProperty("sys")]
        public Sys System { get; set; } 

        [JsonProperty("weather")]
        public List<Weather> Weather { get; set; } 

        [JsonProperty("main")]
        public Main MainWeather { get; set; } 

        [JsonProperty("wind")]
        public Wind Wind { get; set; } 

        [JsonProperty("clouds")]
        public Clouds Clouds { get; set; } 

        [JsonProperty("id")]
        public int CityId { get; set; } 

        [JsonProperty("name")]
        public string Name { get; set; } 

        [JsonProperty("dt_txt")]
        public string Date { get; set; }

        [JsonIgnore]
        public string DisplayDate => DateTime.Parse(Date).ToLocalTime().ToString("g");
        [JsonIgnore]
        public string DisplayTemp => $"Temp: {MainWeather?.Temperature ?? 0}° {Weather?[0]?.Main ?? string.Empty}";
        [JsonIgnore]
        public string DisplayWind => $"Wind: {Wind.Speed} Km/h - Direction: {Wind.WindDirectionDegrees}° ";

    }
}
