﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace XamarinDoTempo.Models
{
    public class Coord
    {
        [JsonProperty("lon")]
        public double Longitude { get; set; } 

        [JsonProperty("lat")]
        public double Latitude { get; set; } 
    }
}
