﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace XamarinDoTempo.Models
{
    public class Clouds
    {

        [JsonProperty("all")]
        public int CloudinessPercent { get; set; } 
    }
}
