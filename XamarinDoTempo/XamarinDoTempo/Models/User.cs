﻿namespace XamarinDoTempo.Models
{
    using SQLite.Net.Attributes;

    public class User
    {
        [PrimaryKey]
        public int UserId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public override int GetHashCode()
        {
            return UserId;
        }
    }
}