﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace XamarinDoTempo.Models
{
    public class Sys
    {

        [JsonProperty("country")]
        public string Country { get; set; } 
    }
}
