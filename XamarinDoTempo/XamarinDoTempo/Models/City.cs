﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite.Net.Attributes;

namespace XamarinDoTempo.Models
{
    public class City
    {
        [JsonProperty("id")]
        [PrimaryKey]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        //[JsonProperty("coord")]
        //public Coord Coord { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("population")]
        public int? Population { get; set; }
        //[JsonProperty("sys")]
        //public Sys Sys { get; set; }

        public double? lat { get; set; }

        public double? lon { get; set; }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}
