﻿

namespace XamarinDoTempo.Services
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Models;
    using Plugin.Connectivity;

    public class ApiServices
    {
        public async Task<Response> CheckConnection()
        {
            //detecta se nao ha internet
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "please see your interner config."
                };
            }

            //deteta se ha conexao e nao consegue ir a internet
            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

            if (!isReachable)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "please see your interner connection."
                };
            }



            //se der tudo responde positivo
            return new Response
            {
                IsSucess = true,
                Message = "Ok"
            };
        }
        //crio a class tokenresult
        //passa os dados url, user e pass para apanhar o token
        /// <summary>
        /// apanho o token
        /// </summary>
        /// <param name="urlBase"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetToken(string urlBase, string username, string password)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(urlBase);
                //chama o token, dizendo o grant_type, user e pass e o encoding
                var response = await client.PostAsync("Token", new StringContent(
                    string.Format("grant_type=password&userName={0}&password={1}", username, password),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJson);

                return result;
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// apanho a lista de paises
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetList<T>(string urlBase, string servicePrefix, string controller)
        {
            try
            {
                var client = new HttpClient();


                client.BaseAddress = new Uri(urlBase);

                //junta o url com o prefixo
                var url = string.Format("{0}{1}", servicePrefix, controller);

                //envia o url completo
                var response = await client.GetAsync(url);

                //RECEBE O JSON
                var result = await response.Content.ReadAsStringAsync();

                //se nao retornar
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result

                    };
                }
                //recebe o objecto e desserializa
                var list = JsonConvert.DeserializeObject<List<T>>(result);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// apanho o tempo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetListWeather<T>(string urlBase, string servicePrefix, string controller,int id)
        {
            try
            {
                var client = new HttpClient();
                string key = "1238115f91c7f31a544b9ac16dd72b66";
                string units = "metric";

                client.BaseAddress = new Uri(urlBase);

                //junta o url com o prefixo
                var url = string.Format("{0}{1}{2}&units={3}&APPID={4}", servicePrefix, controller, id, units, key);

                //envia o url completo
                var response = await client.GetAsync(url);

                //RECEBE O JSON
                var result = await response.Content.ReadAsStringAsync();

                //se nao retornar
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result

                    };
                }

                var newResponse = JsonConvert.DeserializeObject<Response>(result);

                if (!response.IsSuccessStatusCode)
                {
                    newResponse.IsSucess = false;

                    return newResponse;
                }

                //recebe o objecto e desserializa
                var list = JsonConvert.DeserializeObject<T>(result);

                
                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }
            catch (Exception e)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = e.Message
                };
            }
        }

        /// <summary>
        /// faz o registo na bd
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="servicePrefix"></param>
        /// <param name="controller"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Response> AddModelAsync<T>(string urlBase, string servicePrefix, string controller, T model)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(urlBase);

                var url = string.Format("{0}{1}", servicePrefix, controller);

                var data = JsonConvert.SerializeObject(model);
                var content = new StringContent(data, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                response = await client.PostAsync(url, content);

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = response.ReasonPhrase,
                        Code = response.StatusCode
                    };
                }

                return new Response
                {
                    IsSucess = true,
                    Message = data,
                    Code = response.StatusCode
                };

            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response> GetForecast<T>(string ForecaseUri , int id)
        {
            using (var client = new HttpClient())
            {
                var url = string.Format(ForecaseUri, id);
                var json = await client.GetStringAsync(url);

                if (string.IsNullOrWhiteSpace(json))
                    return null;

                var list = JsonConvert.DeserializeObject<T>(json);

                return new Response
                {
                    IsSucess = true,
                    Message = "Ok",
                    Result = list
                };
            }

        }


    }
}
