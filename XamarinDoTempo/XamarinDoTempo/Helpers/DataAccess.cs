﻿
namespace XamarinDoTempo.Helpers
{

    using System;
    using System.Collections.Generic;
    using System.IO;
    using Xamarin.Forms;
    using Interfaces;
    using Models;
    using SQLite.Net;
    using SQLiteNetExtensions.Extensions;
    using System.Linq;

    public class DataAccess : IDisposable
    {
        private SQLiteConnection connection;

        public DataAccess()
        {
            var config = DependencyService.Get<IConfig>();

            //ligacao a tabela e nome da tabela
            this.connection = new SQLite.Net.SQLiteConnection(
                config.Platform,
                Path.Combine(config.DirectoryDB,
                    "tempo.db3"));

            //nome do objecto que vai para a tabela
            connection.CreateTable<User>();
            connection.CreateTable<City>();

        }

        //inserir
        public void Insert<T>(T model)
        {
            this.connection.Insert(model);
        }

        //update
        public void Update<T>(T model)
        {
            this.connection.Update(model);
        }

        //apagar
        public void Delete<T>(T model)
        {
            this.connection.Delete(model);
        }

        //encontra o primeiro que aparece
        public T First<T>(bool WithChildren) where T : class
        {
            if (WithChildren)
            {

                return connection.GetAllWithChildren<T>().FirstOrDefault();
            }
            else
            {
                return connection.Table<T>().FirstOrDefault();
            }
        }

        //procurar se existe na lista
        public List<T> GetList<T>(bool WithChildren) where T : class
        {
            if (WithChildren)
            {
                return connection.GetAllWithChildren<T>().ToList();
            }
            else
            {
                return connection.Table<T>().ToList();
            }
        }

        //procurar
        public T Find<T>(int pk, bool WithChildren) where T : class
        {
            if (WithChildren)
            {
                return connection.GetAllWithChildren<T>().FirstOrDefault(m => m.GetHashCode() == pk);
            }
            else
            {
                return connection.Table<T>().FirstOrDefault(m => m.GetHashCode() == pk);
            }
        }

        public void Dispose()
        {
            connection.Dispose();
        }


    }
}
